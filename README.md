# airplane-ticket

Simple Java-based Airplane Ticket Calculator



Sample Input & Output

```
Selamat Datang di Airplane AIR
Pilih tujuan penerbangan : 
------------------------------
1. Europe - Asia
2. Asia - Europe
3. Europe - US
Masukan nomor yang akan dipilih : 1_
Pilih Maskapai yang akan digunakan : 
1. AirBus 380
2. Boeing 747
3. Boeing 787
Masukan nomor yang akan dipilih : 1_
Pilih kelas pesawat
1. Ekonomi
2. Business
3. First Class
Masukan nomor yang akan dipilih : 1_
Harga tiket ekonomi  :500
Tempat duduk ekonomi :487
Bagasi ekonomi       :20.0
```

