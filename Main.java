import java.util.Scanner;

public class Main {
    public static void Pilihan(int tujuan, int maskapai, int kelas) {
        Plane pesawat = new Plane();
        if (tujuan == 1 && maskapai == 1 && kelas == 1)  {
            pesawat.economy(500,750);
        } else if (tujuan == 1 && maskapai == 2 && kelas == 1 ) {
            pesawat.economy(500,529);
        } else if (tujuan == 1 && maskapai == 3 && kelas == 1) {
            pesawat.economy(500,248);
        } else if (tujuan == 2 && maskapai == 1 && kelas == 1) {
            pesawat.economy(600,750);
        } else if (tujuan == 2 && maskapai == 2 && kelas == 1) {
            pesawat.economy(600,529);
        } else if (tujuan == 2 && maskapai == 3 && kelas == 1) {
            pesawat.economy(600,248);
        } else if (tujuan == 3 && maskapai == 1 && kelas == 1) {
            pesawat.economy(650,750);
        } else if (tujuan == 3 && maskapai == 2 && kelas == 1) {
            pesawat.economy(650,529);
        } else if (tujuan == 3 && maskapai == 3 && kelas == 1) {
            pesawat.economy(650,248);
        } else if (tujuan == 1 && maskapai == 1 && kelas == 2)  {
            pesawat.business(500,750);
        } else if (tujuan == 1 && maskapai == 2 && kelas == 2 ) {
            pesawat.business(500,529);
        } else if (tujuan == 1 && maskapai == 3 && kelas == 2) {
            pesawat.business(500,248);
        } else if (tujuan == 2 && maskapai == 1 && kelas == 2) {
            pesawat.business(600,750);
        } else if (tujuan == 2 && maskapai == 2 && kelas == 2) {
            pesawat.business(600,529);
        } else if (tujuan == 2 && maskapai == 3 && kelas == 2) {
            pesawat.business(600,248);
        } else if (tujuan == 3 && maskapai == 1 && kelas == 2) {
            pesawat.business(650,750);
        } else if (tujuan == 3 && maskapai == 2 && kelas == 2) {
            pesawat.business(650,529);
        } else if (tujuan == 3 && maskapai == 3 && kelas == 2) {
            pesawat.business(650,248);
        } else if (tujuan == 1 && maskapai == 1 && kelas == 3)  {
            pesawat.firstClass(500,750);
        } else if (tujuan == 1 && maskapai == 2 && kelas == 3) {
            pesawat.firstClass(500,529);
        } else if (tujuan == 1 && maskapai == 3 && kelas == 3) {
            pesawat.firstClass(500,248);
        } else if (tujuan == 2 && maskapai == 1 && kelas == 3) {
            pesawat.firstClass(600,750);
        } else if (tujuan == 2 && maskapai == 2 && kelas == 3) {
            pesawat.firstClass(600,529);
        } else if (tujuan == 2 && maskapai == 3 && kelas == 3) {
            pesawat.firstClass(600,248);
        } else if (tujuan == 3 && maskapai == 1 && kelas == 3) {
            pesawat.firstClass(650,750);
        } else if (tujuan == 3 && maskapai == 2 && kelas == 3) {
            pesawat.firstClass(650,529);
        } else if (tujuan == 3 && maskapai == 3 && kelas == 3) {
            pesawat.firstClass(650,248);
        } else {
            System.out.println("Pilihan anda tidak ada.");
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Plane pesawat = new Plane();
        System.out.println("Selamat Datang di Airplane AIR");
        System.out.println("Pilih tujuan penerbangan : ");
        System.out.println("------------------------------");
        System.out.println("1. Europe - Asia");
        System.out.println("2. Asia - Europe");
        System.out.println("3. Europe - US");
        System.out.print("Masukan nomor yang akan dipilih : ");
        int tujuan = scan.nextInt();
        System.out.println("Pilih Maskapai yang akan digunakan : ");
        System.out.println("1. AirBus 380");
        System.out.println("2. Boeing 747");
        System.out.println("3. Boeing 787");
        System.out.print("Masukan nomor yang akan dipilih : ");
        int maskapai = scan.nextInt();
        System.out.println("Pilih kelas pesawat");
        System.out.println("1. Ekonomi");
        System.out.println("2. Business");
        System.out.println("3. First Class");
        System.out.print("Masukan nomor yang akan dipilih : ");
        int kelas = scan.nextInt();       
        Pilihan(tujuan,maskapai,kelas);
        scan.close();
    }
}