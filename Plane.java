class Plane {
    /**
     * Calculate price for economy class
     * @param ticketPrice, planeSeats
     */
    public void economy(double ticketPrice, double planeSeats){
        double totalPrice;
        double totalSeats;
        double totalLuggage;
        totalPrice=(ticketPrice*100)/100;
        totalSeats=(planeSeats*65)/100;
        totalLuggage=(20*100)/100;
        System.out.println("Harga tiket ekonomi  :" + (int)totalPrice);
        System.out.println("Tempat duduk ekonomi :" + (int)totalSeats);
        System.out.println("Bagasi ekonomi       :" + (int)totalLuggage);
    }

    /**
     * Calculate price for business class
     * @param ticketPrice, planeSeats
     */
    public void business(float ticketPrice, float planeSeats){
        float totalPrice;
        float totalSeats;
        float totalLuggage;
        totalPrice=(ticketPrice*225)/100;
        totalSeats=(planeSeats*25)/100;
        totalLuggage=(20*200)/100;
        System.out.println("Harga tiket bisnis   :" + (int)totalPrice);
        System.out.println("Tempat duduk bisnis  :" + (int)totalSeats);
        System.out.println("Bagasi bisnis        :" + (int)totalLuggage);
    }

    /**
     * Calculate price for first class
     * @param ticketPrice, planeSeats
     */
    public void firstClass(float ticketPrice, float planeSeats){
        float totalPrice;
        float totalSeats;
        float totalLuggage;
        totalPrice=(ticketPrice*300)/100;
        totalSeats=(planeSeats*10)/100;
        totalLuggage=(20*250)/100;
        System.out.println("Harga tiket first class  :" + (int)totalPrice);
        System.out.println("Tempat duduk first class :" + (int)totalSeats);
        System.out.println("Bagasi first class       :" + (int)totalLuggage);
    }
}
